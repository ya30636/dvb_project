# CMake generated Testfile for 
# Source directory: /home/parallels/gr-DVB_project
# Build directory: /home/parallels/gr-DVB_project/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(include/DVB_project)
SUBDIRS(lib)
SUBDIRS(swig)
SUBDIRS(python)
SUBDIRS(grc)
SUBDIRS(apps)
SUBDIRS(docs)
