var files =
[
    [ "api.h", "api_8h.html", "api_8h" ],
    [ "my_conversion.h", "my__conversion_8h.html", [
      [ "my_conversion", "classgr_1_1DVB__project_1_1my__conversion.html", "classgr_1_1DVB__project_1_1my__conversion" ]
    ] ],
    [ "my_conversion_impl.h", "my__conversion__impl_8h.html", [
      [ "my_conversion_impl", "classgr_1_1DVB__project_1_1my__conversion__impl.html", "classgr_1_1DVB__project_1_1my__conversion__impl" ]
    ] ],
    [ "my_correlation.h", "my__correlation_8h.html", [
      [ "my_correlation", "classgr_1_1DVB__project_1_1my__correlation.html", "classgr_1_1DVB__project_1_1my__correlation" ]
    ] ],
    [ "my_correlation_impl.h", "my__correlation__impl_8h.html", [
      [ "my_correlation_impl", "classgr_1_1DVB__project_1_1my__correlation__impl.html", "classgr_1_1DVB__project_1_1my__correlation__impl" ]
    ] ],
    [ "my_decision.h", "my__decision_8h.html", [
      [ "my_decision", "classgr_1_1DVB__project_1_1my__decision.html", "classgr_1_1DVB__project_1_1my__decision" ]
    ] ],
    [ "my_decision_impl.h", "my__decision__impl_8h.html", [
      [ "my_decision_impl", "classgr_1_1DVB__project_1_1my__decision__impl.html", "classgr_1_1DVB__project_1_1my__decision__impl" ]
    ] ],
    [ "my_pccc_decoder.h", "my__pccc__decoder_8h.html", [
      [ "my_pccc_decoder", "classgr_1_1DVB__project_1_1my__pccc__decoder.html", "classgr_1_1DVB__project_1_1my__pccc__decoder" ]
    ] ],
    [ "my_pccc_decoder_impl.h", "my__pccc__decoder__impl_8h.html", [
      [ "my_pccc_decoder_impl", "classgr_1_1DVB__project_1_1my__pccc__decoder__impl.html", "classgr_1_1DVB__project_1_1my__pccc__decoder__impl" ]
    ] ]
];