var hierarchy =
[
    [ "block", null, [
      [ "gr::DVB_project::my_pccc_decoder", "classgr_1_1DVB__project_1_1my__pccc__decoder.html", [
        [ "gr::DVB_project::my_pccc_decoder_impl", "classgr_1_1DVB__project_1_1my__pccc__decoder__impl.html", null ]
      ] ]
    ] ],
    [ "sync_block", null, [
      [ "gr::DVB_project::my_correlation", "classgr_1_1DVB__project_1_1my__correlation.html", [
        [ "gr::DVB_project::my_correlation_impl", "classgr_1_1DVB__project_1_1my__correlation__impl.html", null ]
      ] ]
    ] ],
    [ "sync_decimator", null, [
      [ "gr::DVB_project::my_conversion", "classgr_1_1DVB__project_1_1my__conversion.html", [
        [ "gr::DVB_project::my_conversion_impl", "classgr_1_1DVB__project_1_1my__conversion__impl.html", null ]
      ] ],
      [ "gr::DVB_project::my_decision", "classgr_1_1DVB__project_1_1my__decision.html", [
        [ "gr::DVB_project::my_decision_impl", "classgr_1_1DVB__project_1_1my__decision__impl.html", null ]
      ] ]
    ] ]
];