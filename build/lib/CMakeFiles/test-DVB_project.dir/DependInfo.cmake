# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/parallels/gr-DVB_project/lib/qa_DVB_project.cc" "/home/parallels/gr-DVB_project/build/lib/CMakeFiles/test-DVB_project.dir/qa_DVB_project.cc.o"
  "/home/parallels/gr-DVB_project/lib/test_DVB_project.cc" "/home/parallels/gr-DVB_project/build/lib/CMakeFiles/test-DVB_project.dir/test_DVB_project.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/parallels/gr-DVB_project/build/lib/CMakeFiles/gnuradio-DVB_project.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../lib"
  "../include"
  "lib"
  "include"
  "/usr/local/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
