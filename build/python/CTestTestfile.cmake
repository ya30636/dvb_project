# CMake generated Testfile for 
# Source directory: /home/parallels/gr-DVB_project/python
# Build directory: /home/parallels/gr-DVB_project/build/python
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(qa_my_decision "/bin/sh" "/home/parallels/gr-DVB_project/build/python/qa_my_decision_test.sh")
ADD_TEST(qa_my_conversion "/bin/sh" "/home/parallels/gr-DVB_project/build/python/qa_my_conversion_test.sh")
ADD_TEST(qa_my_correlation "/bin/sh" "/home/parallels/gr-DVB_project/build/python/qa_my_correlation_test.sh")
