# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/parallels/gr-DVB_project

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/parallels/gr-DVB_project/build

# Utility rule file for DVB_project_swig_swig_doc.

# Include the progress variables for this target.
include swig/CMakeFiles/DVB_project_swig_swig_doc.dir/progress.make

swig/CMakeFiles/DVB_project_swig_swig_doc: swig/DVB_project_swig_doc.i

swig/DVB_project_swig_doc.i: swig/DVB_project_swig_doc_swig_docs/xml/index.xml
	$(CMAKE_COMMAND) -E cmake_progress_report /home/parallels/gr-DVB_project/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating python docstrings for DVB_project_swig_doc"
	cd /home/parallels/gr-DVB_project/docs/doxygen && /usr/bin/python2 -B /home/parallels/gr-DVB_project/docs/doxygen/swig_doc.py /home/parallels/gr-DVB_project/build/swig/DVB_project_swig_doc_swig_docs/xml /home/parallels/gr-DVB_project/build/swig/DVB_project_swig_doc.i

swig/DVB_project_swig_doc_swig_docs/xml/index.xml: swig/_DVB_project_swig_doc_tag
	$(CMAKE_COMMAND) -E cmake_progress_report /home/parallels/gr-DVB_project/build/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating doxygen xml for DVB_project_swig_doc docs"
	cd /home/parallels/gr-DVB_project/build/swig && ./_DVB_project_swig_doc_tag
	cd /home/parallels/gr-DVB_project/build/swig && /usr/bin/doxygen /home/parallels/gr-DVB_project/build/swig/DVB_project_swig_doc_swig_docs/Doxyfile

DVB_project_swig_swig_doc: swig/CMakeFiles/DVB_project_swig_swig_doc
DVB_project_swig_swig_doc: swig/DVB_project_swig_doc.i
DVB_project_swig_swig_doc: swig/DVB_project_swig_doc_swig_docs/xml/index.xml
DVB_project_swig_swig_doc: swig/CMakeFiles/DVB_project_swig_swig_doc.dir/build.make
.PHONY : DVB_project_swig_swig_doc

# Rule to build all files generated by this target.
swig/CMakeFiles/DVB_project_swig_swig_doc.dir/build: DVB_project_swig_swig_doc
.PHONY : swig/CMakeFiles/DVB_project_swig_swig_doc.dir/build

swig/CMakeFiles/DVB_project_swig_swig_doc.dir/clean:
	cd /home/parallels/gr-DVB_project/build/swig && $(CMAKE_COMMAND) -P CMakeFiles/DVB_project_swig_swig_doc.dir/cmake_clean.cmake
.PHONY : swig/CMakeFiles/DVB_project_swig_swig_doc.dir/clean

swig/CMakeFiles/DVB_project_swig_swig_doc.dir/depend:
	cd /home/parallels/gr-DVB_project/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/parallels/gr-DVB_project /home/parallels/gr-DVB_project/swig /home/parallels/gr-DVB_project/build /home/parallels/gr-DVB_project/build/swig /home/parallels/gr-DVB_project/build/swig/CMakeFiles/DVB_project_swig_swig_doc.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : swig/CMakeFiles/DVB_project_swig_swig_doc.dir/depend

