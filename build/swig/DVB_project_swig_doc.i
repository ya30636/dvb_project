
/*
 * This file was automatically generated using swig_doc.py.
 *
 * Any changes to it will be lost next time it is regenerated.
 */




%feature("docstring") gr::DVB_project::my_conversion "<+description of block+>"

%feature("docstring") gr::DVB_project::my_conversion::make "Return a shared_ptr to a new instance of DVB_project::my_conversion.

To avoid accidental use of raw pointers, DVB_project::my_conversion's constructor is in a private implementation class. DVB_project::my_conversion::make is the public interface for creating new instances.

Params: (NONE)"

%feature("docstring") gr::DVB_project::my_correlation "<+description of block+>"

%feature("docstring") gr::DVB_project::my_correlation::make "Return a shared_ptr to a new instance of DVB_project::my_correlation.

To avoid accidental use of raw pointers, DVB_project::my_correlation's constructor is in a private implementation class. DVB_project::my_correlation::make is the public interface for creating new instances.

Params: (symbols, filter, sps, accuracy, threshold, transmit_power)"

%feature("docstring") gr::DVB_project::my_correlation::symbols "

Params: (NONE)"

%feature("docstring") gr::DVB_project::my_correlation::set_symbols "

Params: (symbols)"

%feature("docstring") gr::DVB_project::my_decision "<+description of block+>"

%feature("docstring") gr::DVB_project::my_decision::make "Return a shared_ptr to a new instance of DVB_project::my_decision.

To avoid accidental use of raw pointers, DVB_project::my_decision's constructor is in a private implementation class. DVB_project::my_decision::make is the public interface for creating new instances.

Params: (sps, preamble_length, burst_length, phase_offset)"

%feature("docstring") gr::DVB_project::my_pccc_decoder "<+description of block+>"

%feature("docstring") gr::DVB_project::my_pccc_decoder::make "Return a shared_ptr to a new instance of DVB_project::my_pccc_decoder.

To avoid accidental use of raw pointers, DVB_project::my_pccc_decoder's constructor is in a private implementation class. DVB_project::my_pccc_decoder::make is the public interface for creating new instances.

Params: (INPUT_TYPE, blocklength, STi0, STiK, coderate, repetitions, SISO_TYPE)"