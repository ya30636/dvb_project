/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_DVB_PROJECT_MY_CORRELATION_H
#define INCLUDED_DVB_PROJECT_MY_CORRELATION_H

#include <DVB_project/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace DVB_project {

    /*!
     * \brief <+description of block+>
     * \ingroup DVB_project
     *
     */
    class DVB_PROJECT_API my_correlation : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<my_correlation> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of DVB_project::my_correlation.
       *
       * To avoid accidental use of raw pointers, DVB_project::my_correlation's
       * constructor is in a private implementation
       * class. DVB_project::my_correlation::make is the public interface for
       * creating new instances.
       */
      static sptr make(const std::vector<gr_complex> &symbols,
                       const std::vector<float> &filter,
                       unsigned int sps, unsigned int accuracy, float threshold, float transmit_power);

      virtual std::vector<gr_complex> symbols() const = 0;
      virtual void set_symbols(const std::vector<gr_complex> &symbols) = 0;
    };

  } // namespace DVB_project
} // namespace gr

#endif /* INCLUDED_DVB_PROJECT_MY_CORRELATION_H */

