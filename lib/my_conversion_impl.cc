/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "my_conversion_impl.h"

namespace gr {
  namespace DVB_project {

    my_conversion::sptr
    my_conversion::make()
    {
      return gnuradio::get_initial_sptr
        (new my_conversion_impl());
    }

    /*
     * The private constructor
     */
    my_conversion_impl::my_conversion_impl()
      : gr::sync_decimator("my_conversion",
              gr::io_signature::make(1, 1, sizeof(char)),
              gr::io_signature::make(1, 1, sizeof(float)), 2)
    {}

    /*
     * Our virtual destructor.
     */
    my_conversion_impl::~my_conversion_impl()
    {
    }

    int
    my_conversion_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const char *in = (const char*) input_items[0];
      float *out = (float*) output_items[0];

     for(int i = 0; i < noutput_items; i++)
      {
        out[i] = GetTwosComplement( in[i*2], in[i*2+1] );
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

    float my_conversion_impl::GetTwosComplement( const char a, const char b)
    {
      int c = ( ( a<<8 ) | ( 0x00FF & b ) );
      return (float)c ;
    }

  } /* namespace DVB_project */
} /* namespace gr */

