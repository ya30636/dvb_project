/* -*- c++ -*- */
/*
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <gnuradio/math.h>
#include "my_correlation_impl.h"
#include <volk/volk.h>
#include <boost/format.hpp>

namespace gr {
  namespace DVB_project {

    my_correlation::sptr
    my_correlation::make(const std::vector<gr_complex> &symbols,
                       const std::vector<float> &filter,
                       unsigned int sps, unsigned int accuracy, float threshold, float transmit_power)
    {
      return gnuradio::get_initial_sptr
        (new my_correlation_impl(symbols, filter, sps, accuracy, threshold, transmit_power));
    }

    /*
     * The private constructor
     */
    my_correlation_impl::my_correlation_impl(const std::vector<gr_complex> &symbols,
                       const std::vector<float> &filter,
                       unsigned int sps, unsigned int accuracy, float threshold, float transmit_power)
      : gr::sync_block("my_correlation",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(3, 3, sizeof(gr_complex)))
    {
      // initial
      N = accuracy;
      d_threshold = threshold;
      d_transpower = transmit_power;
      d_time = 0;
      d_last_index = 0;
      d_sps = sps;
      tp=0;

      // initial preamble
      std::vector<gr_complex> d_temp = symbols;

      // modulate preamble
      std::vector<gr_complex> d_preamble;

      // initial modulate preamble
      d_preamble.resize(d_sps*symbols.size(), 0);

     // modulate
     //********************************************//
     for (int i=0; i< d_temp.size(); i++){
        if (d_temp[i].real() == 0){
            d_preamble[d_sps*i].real() = 1;
            d_preamble[d_sps*i].imag() = 1;
        }

      else if (d_temp[i].real() == 1){
          d_preamble[d_sps*i].real() = -1;
          d_preamble[d_sps*i].imag() =  1;
       }


      else if (d_temp[i].real() == 2){
          d_preamble[d_sps*i].real() = -1;
          d_preamble[d_sps*i].imag() = -1;
        }

      else{
          d_preamble[d_sps*i].real() =  1;
          d_preamble[d_sps*i].imag() = -1;
       }

    }
    //********************************************//

     // initial  preamble
     d_symbols.resize(filter.size()+d_sps*symbols.size()-1, 0);

     // set preamble size
     npreamble = d_symbols.size();

     // modulate preamble do convolution with rrc
     conv(filter, d_preamble, d_symbols);

     // normalized preamble
     float total_power = 0;
     for(int t=0; t<npreamble; t++){
      total_power += abs( d_symbols[t]*conj(d_symbols[t]));
     }
      for(int t=0; t<npreamble; t++){
        d_symbols[t].real() = d_symbols[t].real()*(sqrt(npreamble/total_power));
        d_symbols[t].imag() = d_symbols[t].imag()*(sqrt(npreamble/total_power));
      }

      //  set d_filter, for correlation
      std::reverse(d_symbols.begin(), d_symbols.end());
      d_filter = new gr::filter::kernel::fft_filter_ccc(1, d_symbols); // % set d_filter

      // set correlate number of noutput_items ( for block convolution )
      int nsamples;
      nsamples = d_filter->set_taps(d_symbols);
      set_output_multiple(nsamples);
      set_max_noutput_items(nsamples);

      // for power
      //in_temp.resize(npreamble,0);
      temp_p.resize(npreamble-nsamples/N,0);

      // save the skip points
      d_value = 0;

      // burst counter
      d_counter = 0;

      // trigger if cut the peak value
      d_overflow = 0;


      //set_history(d_symbols.size()+1); // corr_est
      set_history(d_filter->ntaps());
    }

    /*
     * Our virtual destructor.
     */
    my_correlation_impl::~my_correlation_impl()
    {
      delete d_filter;
    }

    std::vector<gr_complex>
    my_correlation_impl::symbols() const
    {
      return d_symbols;
    }

    void
    my_correlation_impl::set_symbols(const std::vector<gr_complex> &symbols)
    {
      gr::thread::scoped_lock lock(d_setlock);
      d_symbols = symbols;
      d_filter->set_taps(symbols);
      set_history(d_filter->ntaps());
    }

    void
    my_correlation_impl::conv(const std::vector<float> & A, const std::vector<gr_complex> & B, std::vector<gr_complex> & C)
    {
      int lenA, lenB;
      int nconv;
      int i, j, i1;
      float tmp_real;
      float tmp_imag;

      //allocated convolution length
      lenA = A.size();
      lenB = B.size();
      nconv = C.size();

      //convolution process
      for (i=0; i<nconv; i++)
      {
        i1 = i;
        tmp_real = 0.0;
        tmp_imag = 0.0;
        for (j=0; j<lenB; j++)
        {
          if(i1>=0 && i1<lenA){
            tmp_real = tmp_real + (A[i1]*B[j].real());
            tmp_imag = tmp_real;
        }

          i1 = i1-1;
          C[i].real() = tmp_real;
          C[i].imag() = tmp_imag;
        }
      }
    }

    int
    my_correlation_impl::work(int noutput_items,
                                     gr_vector_const_void_star &input_items,
                                     gr_vector_void_star &output_items)
    {
      gr::thread::scoped_lock lock(d_setlock);

      const gr_complex *in = (gr_complex *)input_items[0];
      gr_complex *out = (gr_complex*)output_items[0];
      gr_complex *corr = (gr_complex*)output_items[1];
      gr_complex *corr_nor = (gr_complex*)output_items[2];
      std::vector<gr_complex> power_total(noutput_items);

      // for tag
      unsigned int hist_len = history() - 1;

      // copy input to output
      memcpy(out, &in[0], sizeof(gr_complex)*noutput_items);


      // calculate the total energy of signal in each part, sliding window, N = accuracy
      //****************************************************//
      int npart = noutput_items/N;                               // when N=3, npart = 251
      int nremind = noutput_items - (N-1)*npart;        // when N=3, nremind = 253
      std::vector<float> power;
      power.resize(noutput_items,0);
      float power_temp;
      gr_complex data, data1;
      //std::vector<gr_complex>  in_temp_t(npreamble);

      /*
      for(int a=0; a< npreamble; a++){
        in_temp_t[a] = in_temp[a];
      }
      */

      /*****************************************************************************/
      //std::vector<gr_complex> temp_p(npreamble-npart,0);
      int allmovein= (npreamble/npart)+1;

      for(int  n=1; n<=N; n++){

        if(n<allmovein){
          power_temp = 0;
          for(int a=0; a<= npreamble-n*npart; a++){
            data = temp_p[(n-1)*npart+a];
            power_temp += abs(data*conj(data));
          }
          for(int a=0; a<n*npart; a++){
            data = in[hist_len+a];
            power_temp += abs(data*conj(data));
          }
          // sketch power
          for(int c=0; c<npart; c++){
            power[(n-1)*npart+c] = power_temp;
            power_total[(n-1)*npart+c].real() =  power_temp/npreamble;
            power_total[(n-1)*npart+c].imag() = 0;
          }
        }

        else if( (n>=allmovein)&&(n<N) ){
          power_temp = 0;
          for(int a=0; a<npreamble; a++){
            data = in[hist_len+(n-1)*npart+a];
            power_temp += abs(data*conj(data));
          }
          // sketch power
          for(int c=0; c<npart; c++){
            power[(n-1)*npart+c] = power_temp;
            power_total[(n-1)*npart+c].real() =  power_temp/npreamble;
            power_total[(n-1)*npart+c].imag() = 0;
          }
        }

        else{
          power_temp = 0;
          for(int a=0; a<npreamble; a++){
             data = in[hist_len+(noutput_items-1)-a];
             power_temp += abs(data*conj(data));
          }
           // sketch power
           for(int c=0; c<nremind; c++){
             power[(n-1)*npart+c] = power_temp;
             power_total[(n-1)*npart+c].real() =  power_temp/npreamble;
             power_total[(n-1)*npart+c].imag() = 0;
          }
          for(int a=0; a<temp_p.size(); a++){
            temp_p[a] = in[hist_len+(noutput_items-1)-a];
            //std::cout << "temp_p[" << a << "]=" << temp_p[a] << std::endl;
          }
        }
      }




      /******************************************************************************/



      /*
      int n = 1; // initial

      for(int s=0; s< noutput_items; s++){
          in_temp.erase( in_temp.begin() );
          data =  in[hist_len+s];
          in_temp.push_back( data );

          // non last window
          if(n<N){
            if(s==(n*npart-1)){
              power_temp = 0;
              for(int a=0; a<npreamble; a++){
                data1 = in_temp[a];
                power_temp += abs( data1*conj(data1));
            }
            // sketch power
            for(int c=0; c<npart; c++){
              power[(n-1)*npart+c] = power_temp;
              power_total[(n-1)*npart+c].real() =  power_temp/npreamble;
              power_total[(n-1)*npart+c].imag() = 0;
            }
            n++;
            }
          }

          // last window
          else if(n==N){
            if(s==(noutput_items-1)){
              power_temp = 0;
              for(int a=0; a<npreamble; a++){
                data1 = in_temp[a];
                power_temp += abs( data1*conj(data1));
              }
              // sketch power
              for(int c=0; c<nremind; c++){
                power[(n-1)*npart+c] = power_temp;
                power_total[(n-1)*npart+c].real() =  power_temp/npreamble;
                power_total[(n-1)*npart+c].imag() = 0;
              }
              n++;
            }
          }
          // other case
          else{
            n=N+1;
          }
      }

      /*
      for(int a=0; a< npreamble; a++){
        in_temp[a] = in_temp_t[a];
      }
      */

      tp += 1;
      //****************************************************//




      // Calculate the correlation between signal and preamble symbol
      d_filter->filter(noutput_items, &in[hist_len], corr);  // gnuradio

      // Find the magnitude squared of the correlation
      std::vector<float> corr_mag(noutput_items+1);
      volk_32fc_magnitude_squared_32f(&corr_mag[0], corr, noutput_items);


      // Avoid buffer overflow from nested while, putting a stopper at the end
      corr_mag[noutput_items]=0;
      int i = 0;

      // for skip point, for i += d_sps
      //*****************************************************//
      if(d_value > noutput_items){
        i += noutput_items ;
        d_value -= noutput_items;
      }
      else{
        i += d_value;
        d_value = 0;
      }
      //****************************************************//

      // normalized
      //****************************************************//
      float Ne,De;
      std::vector<float> th_nor(noutput_items+1);
      // power sliding power
      for(size_t a=0; a< noutput_items; a++){
            Ne = sqrt(npreamble) * sqrt((power[a]*(npreamble/npreamble)));
            De = sqrt( corr_mag[a] );
            corr_nor[a].real() = De / Ne  ;
            corr_nor[a].imag() = 0;
            th_nor[a] = De / Ne  ;
            /*
            if(th_nor[a]>=1){
              std::cout << "a=" << a << std::endl;
            }
            */
       }
       //****************************************************//




      // find the peak value and add tag
      while(i < (noutput_items-1)) {

        if( (th_nor[i] >= d_threshold  && power_total[i].real() >= d_transpower) || d_overflow==1  ) {

          // detect the peak value of correlation
          //****************************************************//
          if(d_overflow == 1) {
            // give correct position of peak value
            if(corr_mag[i] > corr_mag[i+1]){
              if(d_pcorr_mag>corr_mag[i]){
                i = i - 1;
                d_overflow = 0;
              }
              else{
                d_overflow = 0;
              }
            }
            else{
              d_overflow = 0;
              while(corr_mag[i] < corr_mag[i+1]){
                  i++;
              }
           }
         }
          else{
            while(corr_mag[i] < corr_mag[i+1]){
                  i++;
              }
           }
           //****************************************************//

          // for cut the peak value of preamble
          if(i==noutput_items-1){
            d_pcorr_mag = corr_mag[i];
            d_overflow = 1;
            break;
          }

          // Adjust the correlation peak position to the initial position of burst
          //int index = i + 63;
          int index;
          if(d_sps==25){
            index = i + 62; // notice matlab is 63
          }
          else{
            index = i + 62/(25/d_sps)  ; // for decimation=5, index = i + 12 has perfect DER.
          }


          //int index = i  + 13; // decimation = 5

          // burst counter
          d_counter ++;

          // add tag in output
          //****************************************************//
          add_item_tag(0, nitems_written(0) + index, pmt::intern("start_est"),
                       pmt::from_uint64(nitems_written(0) + index), pmt::intern(alias()));


          //add_item_tag(1, nitems_written(0) + index, pmt::intern("burst"),
          //             pmt::from_uint64(d_counter), pmt::intern(alias()));


          add_item_tag(1, nitems_written(0) + i, pmt::intern("start_est"),
                       pmt::from_uint64(nitems_written(0) + i), pmt::intern(alias()));

          //add_item_tag(2, nitems_written(1) + index, pmt::intern("burst"),
        //               pmt::from_uint64(d_counter), pmt::intern(alias()));


          add_item_tag(2, nitems_written(1) + i, pmt::intern("start_est"),
                       pmt::from_uint64(nitems_written(0) + i), pmt::intern(alias()));
          //****************************************************//

          i += d_sps;

          // save the skip point larger than noutput_items
          if(i > noutput_items){
            d_value = i - noutput_items;
          }

        } // end if( (th_nor[i] >= d_threshold  && power_total[i].real() >= d_transpower) || d_overflow==1  )
        else
          i++;
      } // end while

      return noutput_items;
    } // end work

  } /* namespace DVB_project */
} /* namespace gr */
