/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_DVB_PROJECT_MY_CORRELATION_IMPL_H
#define INCLUDED_DVB_PROJECT_MY_CORRELATION_IMPL_H

#include <DVB_project/my_correlation.h>
#include <gnuradio/filter/fft_filter.h>

namespace gr {
  namespace DVB_project {

    class my_correlation_impl : public my_correlation
    {
     private:
      
      // initial
      std::vector<gr_complex> d_symbols;
      unsigned int d_sps;
      gr::filter::kernel::fft_filter_ccc  *d_filter;
      float d_center_first_symbol;
      float d_thresh;

      // for block correlation
      int d_overflow;
      int d_counter;
      float multiple;
      int d_last_index;
      int d_value;
      float d_time;

      // for sliding window
      int tp;
      int N;
      int npreamble;
      float d_threshold;
      float d_transpower;
      float d_pcorr_mag;
      std::vector<gr_complex>  in_temp;
      std::vector<gr_complex>  temp_p;
      

     public:
      my_correlation_impl(const std::vector<gr_complex> &symbols,
                       const std::vector<float> &filter,
                       unsigned int sps, unsigned int accuracy, float threshold, float transmit_power);
      ~my_correlation_impl();

      std::vector<gr_complex> symbols() const;
      void set_symbols(const std::vector<gr_complex> &symbols);

      void conv(const std::vector<float> & A, const std::vector<gr_complex> & B, std::vector<gr_complex> & C);

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace DVB_project
} // namespace gr

#endif /* INCLUDED_DVB_PROJECT_MY_CORRELATION_IMPL_H */

