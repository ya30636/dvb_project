/* -*- c++ -*- */
/*
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "my_decision_impl.h"

#include <fstream>
using namespace std;

namespace gr {
  namespace DVB_project {

    my_decision::sptr
    my_decision::make(int sps, int preamble_length, int burst_length, bool phase_offset)
    {
      return gnuradio::get_initial_sptr
        (new my_decision_impl(sps, preamble_length, burst_length, phase_offset));
    }

    /*
     * The private constructor
     */
    my_decision_impl::my_decision_impl(int sps, int preamble_length, int burst_length, bool phase_offset)
      : gr::sync_decimator("my_decision",
                            gr::io_signature::make( 1, 1, sizeof(gr_complex) ),
                            gr::io_signature::make2( 2, 2, sizeof(gr_complex), sizeof(char) ),
                            sps),
        d_sps(sps),
        d_preamble_length(preamble_length),
        d_burst_length(burst_length),
        d_phase_offset(phase_offset)
    {
      offset = 0;
      index_burst = 0;
      index_temp = -1;

      d_time = 0;

      //ofstream outFile( "gnuradio_position.bin", ios::out );

      set_output_multiple(10000);
    }

    /*
     * Our virtual destructor.
     */
    my_decision_impl::~my_decision_impl()
    {
    }

    int
    my_decision_impl::work(int noutput_items,
			  gr_vector_const_void_star &input_items,
			  gr_vector_void_star &output_items)
    {

        // intialize I/O
        const gr_complex *in = (const gr_complex *) input_items[0];
        gr_complex *data = (gr_complex*) output_items[0];
        char *out = (char *) output_items[1];

        // open file
        //ofstream outFile( "gnuradio_position.bin", ios::app );

        // get tags
        get_tags_in_window(d_tags, 0, 0, noutput_items*d_sps );

        // get signal power or noise power (estimate SNR)
        float signal_power = 0;
        float noise_power = 0;
        if( d_tags.empty() ) // view as noise
        {
          for(int i = 0; i< noutput_items* d_sps; i++)
            noise_power = noise_power+ abs(in[i])/(noutput_items* d_sps);
        }
        else // view as signal
        {
          for(int i = 0; i< noutput_items* d_sps; i++)
            signal_power = signal_power+ abs(in[i])/(noutput_items* d_sps);
        }
        //cout<<"signal = "<< signal_power << endl;
        //cout<<"noise = "<< noise_power << endl;
        //cout<<"noutput_items = "<< noutput_items << endl;

        // copy d_tags(tag_t) to s_tags(int)
        std::vector<int> s_tags;
        while (!d_tags.empty())
        {
          s_tags.push_back( pmt::to_uint64( d_tags[0].value ) );
          d_tags.erase (d_tags.begin());
        }

        // get the relative value to nitems_read (for real-time)
        if ( !s_tags.empty() )
        {
          for(int iter = 0; iter < s_tags.size(); iter++)
          {
            s_tags[iter] = s_tags[iter] - nitems_read(0);
          }
        }

        for(int index_out = 0; index_out < noutput_items; index_out++)
        {
            // find the index_in corresponding to index_out
            int index_in = index_out * d_sps + offset;

            // If index is close to tag, save the difference in offset.
            if( !s_tags.empty() )
            {
              if ( s_tags[0] - index_in < d_sps)
              {
                offset = offset + (s_tags[0] - index_in); // find offset
                index_in = s_tags[0]; // syn tag and index
                s_tags.erase (s_tags.begin()); // next tag

                // add new tag to index of burst since the old one is not correct
                index_burst++;
                tag_t new_tag;
                new_tag.key = pmt::mp("index of burst");
                new_tag.offset = nitems_written(0) + index_out + d_preamble_length;
                new_tag.value = pmt::from_uint64(new_tag.offset*2);
                // cout<< nitems_written(0) + index_out << endl;

                //outFile << nitems_written(0) + index_out << endl;

                add_item_tag(0, new_tag);
                add_item_tag(1, new_tag);

                set_tag_propagation_policy(TPP_DONT);

                index_temp = 0;

                // if phase offset was set up, start to initialize.
                if (d_phase_offset == 1)
                {
                  index_temp = 1; // count the index
                  // initialize
                  phase0.clear();
                  phase1.clear();
                  phase2.clear();
                  phase3.clear();
                  out_temp.clear();
                  window_data.clear();
                }

              }
            }

            // sum up the d_sps numbers of sample
            gr_complex sum = 0;

            while( index_in < ( index_out + 1 ) * d_sps + offset )
            {
              if (signal_power)
              {
                sum = sum + in[index_in] / (d_sps*signal_power);
              }
              else
              {
                sum = sum + in[index_in];
              }
              index_in++;
            }

            if ( index_temp > 0 && index_temp <= d_preamble_length )
            {
              GetPhaseFromPreamble(sum, index_temp);

              // maintain window data
              window_data.push_back(sum);

              index_temp++;
            }
            else if ( index_temp == d_preamble_length + 1 )
            {
              // get average rotate angle
              float rotate = GetPhaseAverage();

              // adjust history in the sliding winodws and assign to output
              for (int i = d_preamble_length; i >= 1; i-- )
              {
                data[ index_out-i ] = RotateByPhase( window_data[ d_preamble_length-i ], rotate );
                out[ index_out-i ] = GetMinimumDistances( data[ index_out-i ] );
                out_temp.push_back( out[ index_out-i ] );
              }

              // adjust current data and assign to output
              data[index_out] = RotateByPhase(sum, rotate);
              out[index_out] = GetMinimumDistances(data[index_out]);
              out_temp.push_back(out[index_out]);

              // maintain window data
              window_data.erase(window_data.begin());
              window_data.push_back(sum);

              index_temp++;
            }
            else if ( index_temp > d_preamble_length + 1 )
            {
              // recalculate phase in the new window (data-aided part)
              phase0.clear();
              phase1.clear();
              phase2.clear();
              phase3.clear();
              GetPhaseFromWindow(window_data, out_temp, index_temp - d_preamble_length - 1 );

              // get average rotate angle
              float rotate = GetPhaseAverage();

              // adjust current data and assign to output
              data[index_out] = RotateByPhase(sum, rotate);
              out[index_out] = GetMinimumDistances(data[index_out]);
              out_temp.push_back(out[index_out]);

              // maintain window data
              window_data.erase(window_data.begin());
              window_data.push_back(sum);

              if (index_temp == d_burst_length)
                index_temp = -1;
              else
                index_temp++;
            }
            else if ( index_temp < 0 )
            {
              data[index_out] = sum;
              out[index_out] = 0x04;
            }
            else // default: without phase_offset
            {
              data[index_out] = sum;
              out[index_out] = GetMinimumDistances(sum);
            }

        }

        // Tell runtime system how many output items we produced.
        return noutput_items;
    }



    char my_decision_impl::GetMinimumDistances(const gr_complex &sample)
    {
      if (sample.imag() >= 0 and sample.real() >= 0) {
          return 0x00; // 1+1j
      }
      else if (sample.imag() >= 0 and sample.real() < 0) {
          return 0x01; // -1+1j
      }
      else if (sample.imag() < 0 and sample.real() < 0) {
          return 0x02; // -1-1j
      }
      else if (sample.imag() < 0 and sample.real() >= 0) {
          return 0x03; // 1-1j
      }
    }

    void my_decision_impl::GetPhaseFromPreamble(const gr_complex & sum, const int & index_temp)
    {
      if (index_temp == 1 ||index_temp == 2 ||index_temp == 7 ||index_temp == 8 ||
          index_temp == 14||index_temp == 16||index_temp == 18||index_temp == 19||
          index_temp == 20||index_temp == 21||index_temp == 23||index_temp == 24||
          index_temp == 28||index_temp == 29||index_temp == 31||index_temp == 32||
          index_temp == 37||index_temp == 38||index_temp == 39||index_temp == 41||
          index_temp == 42||index_temp == 45||index_temp == 47||index_temp == 48 )
      {
        phase0.push_back( sum );
      }
      else
      {
        phase2.push_back( sum );
      }
    }

    void my_decision_impl::GetPhaseFromOut(const gr_complex & sum, const char & out)
    {
      switch(out)
      {
        case 0x00:
            phase0.push_back( sum );
            break;
        case 0x01:
            phase1.push_back( sum );
            break;
        case 0x02:
            phase2.push_back( sum );
            break;
        case 0x03:
            phase3.push_back( sum );
            break;
        default:
            break;
      }
    }

    void my_decision_impl::GetPhaseFromWindow(const std::vector<gr_complex> & window_data,
                                            const std::vector<char> & out_temp, const int windowstart)
    {
      for(int index_window = 0; index_window< d_preamble_length; index_window++ )
      {
        GetPhaseFromOut(window_data[index_window], out_temp[index_window + windowstart]);
      }
    }

    float diffmean (const std::vector<gr_complex> &phase, float base)
    {
      if (phase.empty())
      return 0 ;
      else
      {
        gr_complex average_sum= 0;
        for(int i= 0; i< phase.size(); i++){
          average_sum.real()= average_sum.real()+ phase[i].real()/ phase.size();
          average_sum.imag()= average_sum.imag()+ phase[i].imag()/ phase.size();
        }

        float average_phase= std::arg (average_sum) ;

        return ( base - average_phase );
      }
    }

    float my_decision_impl::GetPhaseAverage()
    {
      std::vector<float> phaseoffset(4);
      int flag = 0;
      if (!phase0.empty())
        flag = 0;
      else if (!phase1.empty())
        flag = 1;
      else if (!phase2.empty())
        flag = 2;
      else if (!phase3.empty())
        flag = 3;

      phaseoffset[0] = diffmean (phase0, 0.25 * M_PI);
      phaseoffset[1] = diffmean (phase1, 0.75* M_PI);
      phaseoffset[2] = diffmean (phase2, -0.75* M_PI);
      phaseoffset[3] = diffmean (phase3, -0.25* M_PI);

      for (int i= 0; i< 4; i++)
      {
        if(phaseoffset[i])
          if (i != flag)
            if ( std::abs(phaseoffset[i] - phaseoffset[flag]) > M_PI)
              if ( phaseoffset[flag] > phaseoffset[i] )
                phaseoffset[i] += 2*M_PI;
              else{
                phaseoffset[flag] += 2*M_PI;
                i = flag;
              }
      }

      int phaseoffset_size = 0;
      for (int i= 0; i< 4; i++)
        if(phaseoffset[i]) phaseoffset_size += 1;

      float average_angle = 0;
      for(int i= 0; i< phaseoffset.size(); i++)
        average_angle = average_angle + phaseoffset[i] / phaseoffset_size;

      return average_angle;
    }

    gr_complex my_decision_impl::RotateByPhase(const gr_complex & sum, const float & angle)
    {
      float r = 1.0;
      gr_complex rotate = std::polar (r , angle);
      return sum * rotate;
    }

  } /* namespace DVB_project */
} /* namespace gr */
