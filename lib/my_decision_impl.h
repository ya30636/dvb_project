/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_DVB_PROJECT_MY_DECISION_IMPL_H
#define INCLUDED_DVB_PROJECT_MY_DECISION_IMPL_H

#include <DVB_project/my_decision.h>

namespace gr {
  namespace DVB_project {

    class my_decision_impl : public my_decision
    {
     private:
      int d_sps; 
      int d_preamble_length;
      int d_burst_length;
      bool d_phase_offset;
      std::vector< tag_t > d_tags;

      int offset;
      int index_burst;
      int index_temp;

      std::vector<gr_complex> window_data;
      std::vector<char> out_temp;
      std::vector<gr_complex> phase0;
      std::vector<gr_complex> phase1;
      std::vector<gr_complex> phase2;
      std::vector<gr_complex> phase3;

      float d_time;

     public:
      my_decision_impl(int sps, int preamble_length, int burst_length, bool phase_offset);
      ~my_decision_impl();

      // Where all the action really happens
      int work(int noutput_items,
	       gr_vector_const_void_star &input_items,
	       gr_vector_void_star &output_items);

      char GetMinimumDistances(const gr_complex &sample);
      void GetPhaseFromPreamble(const gr_complex & sum, const int & index_temp);
      void GetPhaseFromOut(const gr_complex & sum, const char & out);
      void GetPhaseFromWindow(const std::vector<gr_complex> & window_data, 
                              const std::vector<char> & out_temp, const int movecount);
      float GetPhaseAverage();
      gr_complex RotateByPhase(const gr_complex & sum, const float & angle);

    };

  } // namespace DVB_project
} // namespace gr

#endif /* INCLUDED_DVB_PROJECT_MY_DECISION_IMPL_H */

