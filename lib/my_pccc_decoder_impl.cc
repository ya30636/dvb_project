/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "my_pccc_decoder_impl.h"

#include <iostream>
#include <gnuradio/trellis/core_algorithms.h>

namespace gr {
  namespace DVB_project {

    my_pccc_decoder::sptr
    my_pccc_decoder::make(int INPUT_TYPE, int blocklength, int STi0, int STiK, int coderate, int repetitions, gr::trellis::siso_type_t SISO_TYPE)
    {
      return gnuradio::get_initial_sptr
        (new my_pccc_decoder_impl(INPUT_TYPE, blocklength, STi0, STiK, coderate, repetitions, SISO_TYPE));
    }

    /*
     * The private constructor
     */
    my_pccc_decoder_impl::my_pccc_decoder_impl(int INPUT_TYPE, int blocklength, int STi0, int STiK, int coderate, int repetitions, gr::trellis::siso_type_t SISO_TYPE)
      : gr::block("my_pccc_decoder",
              gr::io_signature::make(1, 1, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(unsigned char))),
      d_INPUT_TYPE(INPUT_TYPE),
      d_STi0(STi0), d_STiK(STiK),
      d_coderate(coderate),      
      d_blocklength(blocklength),
      d_repetitions(repetitions),
      d_SISO_TYPE(SISO_TYPE)
    {
      d_FSMo = gr::trellis::fsm("/usr/local/share/gnuradio/examples/trellis/fsm_files/awgn1o2_4rsc.fsm");
      d_STo0 = 0;
      d_SToK = -1;
      d_FSMi = gr::trellis::fsm("/usr/local/share/gnuradio/examples/trellis/fsm_files/awgn1o1_4rsc.fsm");
      //d_STi0 =
      //d_STiK =
      d_INTERLEAVER = gr::trellis::interleaver(blocklength,666);
      d_D = 3;
      float table[] = {-1,-1,-1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,1,-1,1,1,1,-1,1,1,1};
      d_TABLE = std::vector<float>(table, table + sizeof(table) / sizeof(float) );

      
      d_METRIC_TYPE = gr::digital::TRELLIS_EUCLIDEAN;
      d_scaling = 0.528298;

      assert(d_FSMo.I() == d_FSMi.I());
      set_relative_rate (1.0 / ((double) d_D));
      set_output_multiple (d_blocklength);
    }
    /*
     * Our virtual destructor.
     */
    my_pccc_decoder_impl::~my_pccc_decoder_impl()
    {
    }

    void
    my_pccc_decoder_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      int input_required = d_D * noutput_items;
      ninput_items_required[0] = input_required;
    }

    int
    my_pccc_decoder_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      //gr::thread::scoped_lock guard(d_setlock);
      int nblocks = noutput_items / d_blocklength;

      float (*p2min)(float, float) = NULL;
      if(d_SISO_TYPE == gr::trellis::TRELLIS_MIN_SUM)
        p2min = &gr::trellis::min;
      else if(d_SISO_TYPE == gr::trellis::TRELLIS_SUM_PRODUCT)
        p2min = &gr::trellis::min_star;
      
      const float *in = (const float *) input_items[0];
      unsigned char *out = (unsigned char *) output_items[0];  

      for (int n=0;n<nblocks;n++) {
        pccc_decoder_combined(d_FSMo, d_STo0, d_SToK,
              d_FSMi, d_STi0, d_STiK,
              d_INTERLEAVER, d_blocklength, d_repetitions,
              p2min,
              d_D,d_TABLE,
              d_METRIC_TYPE,
              d_scaling,
              &(in[n*d_blocklength*d_D]),
              &(out[n*d_blocklength]));
      }

      // Do <+signal processing+>
      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume_each (d_D * noutput_items);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace DVB_project */
} /* namespace gr */

