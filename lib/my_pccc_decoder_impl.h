/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_DVB_PROJECT_MY_PCCC_DECODER_IMPL_H
#define INCLUDED_DVB_PROJECT_MY_PCCC_DECODER_IMPL_H

#include <DVB_project/my_pccc_decoder.h>

namespace gr {
  namespace DVB_project {

    class my_pccc_decoder_impl : public my_pccc_decoder
    {
     private:
      int d_INPUT_TYPE;
      gr::trellis::fsm d_FSMo;
      int d_STo0;
      int d_SToK;
      gr::trellis::fsm d_FSMi;
      int d_STi0;
      int d_STiK;
      int d_coderate;
      gr::trellis::interleaver d_INTERLEAVER;
      int d_blocklength;
      int d_repetitions;
      gr::trellis::siso_type_t d_SISO_TYPE;
      int d_D;
      std::vector<float> d_TABLE;
      gr::digital::trellis_metric_type_t d_METRIC_TYPE;
      float d_scaling;
      std::vector<float> d_buffer;

     public:
      my_pccc_decoder_impl(int INPUT_TYPE, int blocklength, int STi0, int STiK, int coderate, int repetitions, gr::trellis::siso_type_t SISO_TYPE);
      ~my_pccc_decoder_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
		       gr_vector_int &ninput_items,
		       gr_vector_const_void_star &input_items,
		       gr_vector_void_star &output_items);
    };

  } // namespace DVB_project
} // namespace gr

#endif /* INCLUDED_DVB_PROJECT_MY_PCCC_DECODER_IMPL_H */

