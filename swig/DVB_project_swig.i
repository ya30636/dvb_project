/* -*- c++ -*- */

#define DVB_PROJECT_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "DVB_project_swig_doc.i"

%{
#include "DVB_project/my_decision.h"
#include "DVB_project/my_conversion.h"
#include "DVB_project/my_correlation.h"
//#include "DVB_project/my_pccc_decoder.h"
%}


%include "DVB_project/my_decision.h"
GR_SWIG_BLOCK_MAGIC2(DVB_project, my_decision);

%include "DVB_project/my_conversion.h"
GR_SWIG_BLOCK_MAGIC2(DVB_project, my_conversion);

%include "DVB_project/my_correlation.h"
GR_SWIG_BLOCK_MAGIC2(DVB_project, my_correlation);
//%include "DVB_project/my_pccc_decoder.h"
//GR_SWIG_BLOCK_MAGIC2(DVB_project, my_pccc_decoder);
